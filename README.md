# Tic Tac Toe

Clonar repositorio git clone https://wilmarrojas@bitbucket.org/wilmarrojas/tictactoe.git.

Ingresar a la carpeta donde se clonó el repositorio y modificar en el archivo .env  lo parametros de conexío a la base de datos.

DB_CONNECTION= 123
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=DB
DB_USERNAME=name
DB_PASSWORD=pass


## Instalación

Requiere:
[Laravel Websockets](https://beyondco.de/docs/laravel-websockets/getting-started/installation)
[Pusher](https://laravel.com/docs/9.x/broadcasting)


Instalación de paquetes e inicio del servicio.
Websockets
```sh
composer require beyondcode/laravel-websockets
php artisan vendor:publis--provider="BeyondCode\LaravelWebSockets\WebSocketsServiceProvider" --tag="migrations"
```

Ejecutar migraciones y seeders
```sh
php artisan migrate:fresh --seed
```
```sh
php artisan vendor:publish --provider="BeyondCode\LaravelWebSockets\WebSocketsServiceProvider" --tag="config"
```
Pusher
```sh
composer require pusher/pusher-php-server
```
Iniciar servicio web
```sh
php artisan websockets:serve
```
Conectar servicio websocket: acceder a la url http://localhost:8000/laravel-websockets o según corresponda y conectar por el puerto 6001 que esta por defecto.

Instalar laravel echo
Se debe tener instalado npm
```sh
npm install --save-dev laravel-echo pusher-js
```

Ejecutar en un terminal : npm run watch o npm run dev

Iniciar php Artisan
```sh
 php artisan serve
 ```
 
 acceder a la ruta http://localhost:8000/
