<?php

use Illuminate\Support\Facades\Route;

/** Rutas URL */

Route::get('/', 'App\Http\Controllers\HomeController@index');

Route::post('/nuevapartida', 'App\Http\Controllers\NuevapartidaController@nuevapartida');

Route::post('/uniserapartida', 'App\Http\Controllers\UniserapartidaController@consultarpartida');

Route::post('/enviajugada', 'App\Http\Controllers\NotificaJugada@generaevento');

Route::post('/reiniciajuego', 'App\Http\Controllers\ReiniciaPartida@reiniciajuego');


