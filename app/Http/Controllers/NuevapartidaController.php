<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\NuevapartidaModel;
use App\Models\NuevoJugador;
use App\Models\User;

class NuevapartidaController extends Controller
{
    public function nuevapartida(Request $req){
        /**
         * Controlador que genera la nueva partida
         * @param $req
         */
        $codigo = rand(1000, 9999); //genera aleatorio para el id de la partida
        $nombre = $req->input()['name']; //nombre del jugador
        $id = $this->guarda_jugador($nombre); //llama metodo para guardar el nombre del jugador
        $id_partida = $this->guarda_partida($codigo, $id); //llama metodo para guardar la partida
        return view('juego.index')
            ->with('nombre', $nombre)
            ->with('nombre2', 'Esperando...')
            ->with('codigo', $codigo)
            ->with('ficha', 'X'); //retorna vista y pasa archivos
    }

    private function guarda_jugador($nombre){
        /**
         * Metodo para guardar el nombre del jugador
         * @param $nombre String nombre de jugador que crea la partida
         * @return id del jugador creado
         */
        $nuevojugador = new NuevoJugador;
        $nuevojugador->name = $nombre;
        $nuevojugador->save();

        auth()->login(User::where('id', 1)->first()); //autentica el usuario con id 1

        return NuevoJugador::latest('id')->first()['id']; 

    }

    private function guarda_partida($codigo, $id){
        /**
         * Metodo para guardar la partida
         * @param $codigo integer codigo de la partida
         * @param $id integer id del jugador quien crea la partida
         * @return id partida
         */
        $partidaNueva = new NuevapartidaModel;
        $partidaNueva->code = $codigo;
        $partidaNueva->jugador = $id;
        $partidaNueva->invitado = '';
        $partidaNueva->token = '';
        $partidaNueva->created_at = date('Y-m-d h:i:s', time());
        $partidaNueva->save();

        return NuevapartidaModel::latest('id')->first()['id'];;

    }
}
