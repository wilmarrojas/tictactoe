<?php

namespace App\Http\Controllers;
use App\Models\UniserapartidadModel;
use App\Models\NuevoJugador;
use App\Models\User;
use Illuminate\Http\Request;
use App\Events\RealTimeEvent;


class UniserapartidaController extends Controller
{
    public function consultarpartida(Request $req){
        /** Controlador para unirse a partida
         * @param $req array
         */
        $nombre = $req->input()['name'];
        $partida = $req->input()['partida'];
        
        $valida_partida = UniserapartidadModel::where('code', $partida)
            ->first(); //consulta para validar que exista la partida
        if(empty($valida_partida)){
            return back(); //retorna al inicio si no existe la partida
        }
        $jugador2 = $this->crea_usuario($nombre);
        $jugador1 = NuevoJugador::where('id', $valida_partida['jugador'])
            ->first(); //consulta los datos del jugador 1
        $this->actualiza_partida($partida, $jugador2); //llama metodo para actualizar el jugador de la partida
        event(new \App\Events\RealTimeEvent(json_encode(["nombre"=> "$nombre", "partida"=> "$partida"])));    // genera  evento para iniciar a jugar   
        return view('juego.index')
            ->with('nombre', $jugador1['name'])
            ->with('nombre2', $nombre)
            ->with('codigo', $valida_partida['code'])
            ->with('ficha', 'O');
    }

    private function crea_usuario($nombre){
        /**
         * Metodo para guardar el nombre del jugaroq ue se quiere unir a la partida
         * @param $nombre String nombre del jugador
         * @return id del jugador guardado
         */
        $nuevojugador = new NuevoJugador;
        $nuevojugador->name = $nombre;
        $nuevojugador->save();
        auth()->login(User::where('id', 2)
            ->first()); //autentica el usuario 2
        return NuevoJugador::latest('id')
            ->first()['id'];
    }

    private function actualiza_partida($partida, $jugador2){
        /**
         * Metodo para actualizar la partida con el jugador nuevo
         * @param $partida codigo de la partida
         * @param $jugador2 id del jugador 2 (invitado)
         */
        UniserapartidadModel::where('code', $partida)
          ->update(['invitado' => $jugador2]);
    }
}
