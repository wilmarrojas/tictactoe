<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * controlador que carga la vista inicial
     */
    public function index(){
        return view('home.index'); //retorna la vista
    }
}
