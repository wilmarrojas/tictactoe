<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReiniciaPartida extends Controller
{
    public function reiniciajuego(Request $req){
        /**
         * Controlador que generar el evento al reiniciar la partida
         */
        event(new \App\Events\ReiniciaPartida(
            json_encode($req->input())
            )
        );
    }
}
