<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;


class NotificaJugada extends Controller
{
    public function generaevento(Request $req){
        /**
         * Controlador que genera evento para notificar las jugadas realizadas
         */
        event(new \App\Events\NotificaJugada(
                json_encode($req->input())
            )
        );
    }
}
