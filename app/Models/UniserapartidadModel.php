<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UniserapartidadModel extends Model
{
    use HasFactory;
    
    const UPDATED_AT = null; //campo actualizado no requerido
    public $table = "partidas"; //tabla partidas

    //array con atributos asignables
    protected $fillable = [
        'id', 
        'code', 
        'jugador',
        'invitado', 
        'token', 
        'create_at'
    ];
}
