<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NuevoJugador extends Model
{
    use HasFactory;
    public $table = "jugador"; //tabla jugador

    //array con atributos asignables

    protected $fillable = [
        'name',
        'email',
        'token',
        'created_at',
        'update_at'
    ];
}
