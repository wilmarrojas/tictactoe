<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NuevapartidaModel extends Model
{
    /**
     * Modelo tabla partidas
     */
    use HasFactory;
    public $table = "partidas"; //cambiamos nombre tabla
    public $timestamps = false; //modificado para que la fecha no sea obligatorio

    //array con las columna de la tabla

    protected $fillable = [
        'code',
        'jugador',
        'invitado',
        'token',
        'created_at'
    ];
}
