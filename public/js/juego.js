$(document).ready(function(){
    $(".game--restart").hide(); //oculta el boton de reiniciar el juego
    document.querySelectorAll('.cell').forEach(cell => cell.addEventListener('click', handleCellClick)); //llama a funcion despues de dar click sobre un recuadro
    document.querySelector('.game--restart').addEventListener('click', handleRestartGame); //llama funcion al dar click sobre boton reiniciar

  });
  
  const statusDisplay = document.querySelector('.game--status');

  let gameActive = true;
  let currentPlayer = "O";
  var gameActiveP = {
    "X" : false,
    "O" : false,
  };
  let gameState = ["", "", "", "", "", "", "", "", ""];

  const winningMessage = () => `Jugador ${currentPlayer} ha Ganado!`;
  const drawMessage = () => `Juego sin Ganador!`;
  const currentPlayerTurn = () => `Es el turno de ${currentPlayer == "X" ? "O" : "X"}`;

  //statusDisplay.innerHTML = currentPlayerTurn();

  const winningConditions = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
  ];

  function handleCellPlayed(clickedCell, clickedCellIndex) {
      gameState[clickedCellIndex] = currentPlayer;
      clickedCell.innerHTML = currentPlayer;
  }

  function handlePlayerChange() {
      const statusDisplay = document.querySelector('.game--status')
      //currentPlayer = currentPlayer === "X" ? "O" : "X";
      statusDisplay.innerHTML = currentPlayerTurn();
      
  }

  function handleResultValidation(clickedCell) {
      const statusDisplay = document.querySelector('.game--status')
      let roundWon = false;
      for (let i = 0; i <= 7; i++) {
          const winCondition = winningConditions[i];
          let a = gameState[winCondition[0]];
          let b = gameState[winCondition[1]];
          let c = gameState[winCondition[2]];
          if (a === '' || b === '' || c === '') {
              continue;
          }
          if (a === b && b === c) {
              roundWon = true;
              break
          }
      }

      if (roundWon) {
          statusDisplay.innerHTML = winningMessage();
          gameActive = false;
          notifica(clickedCell);
          return;
      }

      let roundDraw = !gameState.includes("");
      if (roundDraw) {
          statusDisplay.innerHTML = drawMessage();
          gameActive = false;
          notifica(clickedCell);
          return;
      }

      handlePlayerChange();
      notifica(clickedCell);
  }

  function handleCellClick(clickedCellEvent) {
      const clickedCell = clickedCellEvent.target;
      const clickedCellIndex = parseInt(clickedCell.getAttribute('data-cell-index'));
      currentPlayer = $('input[name="ficha"]').val();

      if (gameState[clickedCellIndex] !== "" || !gameActiveP[currentPlayer] || !gameActive) {
          return;
      }
      currentPlayer = $('input[name="ficha"]').val();
      handleCellPlayed(clickedCell, clickedCellIndex);
      handleResultValidation(clickedCell);
  }

  function handleRestartGame() {
    var datos = {
        "partida" : $('input[name="partida"]').val(),
        "mensaje": document.getElementsByClassName("game--status")[0].outerText,
        "_token": $('input[name="_token"]').val()
    };
    $.ajax({
        headers: { 'X-CSRF-TOKEN': datos._token },
        url : "/reiniciajuego",
        data : datos,
        datatype : "json",
        method : "POST"
      });
  }

  function notifica(clickedCell){
    var datos = {
        "partida" : $('input[name="partida"]').val(),
        "jugada" : clickedCell.innerHTML,
        "mensaje": document.getElementsByClassName("game--status")[0].outerText,
        "celda": parseInt(clickedCell.getAttribute('data-cell-index')),
        "activo": gameActive,
        "_token": $('input[name="_token"]').val()
      };
    $.ajax({
        headers: { 'X-CSRF-TOKEN': datos._token },
        url : "/enviajugada",
        data : datos,
        datatype : "json",
        method : "POST"
      });
  }
