<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="{{asset('CSS/style.css')}}" rel ="stylesheet">
    <script>
        function nuevapartida(){
            var nombre = document.getElementById("name").value;
            
            if(!nombre.length){
                alert('Debe ingresar un nombre.');

            }else{
                document.location.href = "nuevapartida?nombre="+nombre;
            }
        }
    </script>

    <title>Tic Tac Toe</title>
  </head>
  <body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <div class="cajafuera">
        <div class="menu">
            <h1 class="card-title">Tic Tac Toe</h1>
            <div class="logovai"><img src="{{asset('imges/tictactoe.png')}}"></div>
            <br>
            <div>
                <a href="#" class="card-link" data-bs-toggle="modal" data-bs-target="#modalnuevapartida" data-bs-whatever="@mdo">Nueva Partida</a>
                <a href="#" class="card-link" data-bs-toggle="modal" data-bs-target="#modaluniserapartida" data-bs-whatever="@mdo">Unirse a Partida</a>
            </di>
        </div>
    </div>
    <div class="modal fade" id="modalnuevapartida" tabindex="-1" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLabel">Nueva Partida</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="post" action= "{{url('/nuevapartida')}}">
                @csrf
                    <div class="mb-3">
                        <label for="recipient-name" class="col-form-label">Nombre:</label>
                        <input type="text" class="form-control" id="name" name="name" required value = "">
                    </div>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                    <input type="submit" value="Empezar" class="btn btn-primary">
                </form>
            </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modaluniserapartida" tabindex="-1" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLabel">Nueva Partida</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="post" action= "{{url('/uniserapartida')}}">
                @csrf
                    <div class="mb-2">
                        <label for="recipient-name" class="col-form-label">Codigo partida:</label>
                        <input type="number" class="form-control" id="partida" name="partida" required value = "">
                    </div>
                    <div class="mb-3">
                        <label for="recipient-name" class="col-form-label">Nombre:</label>
                        <input type="text" class="form-control" id="name" name="name" required value = "">
                    </div>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                    <input type="submit" value="Empezar" class="btn btn-primary">
                </form>
            </div>
            </div>
        </div>
    </div>
  </body>
</html>