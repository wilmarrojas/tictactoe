window._ = require('lodash');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

 import Echo from 'laravel-echo';

 window.Pusher = require('pusher-js');

 window.Echo = new Echo({
     broadcaster: 'pusher',
     key: process.env.MIX_PUSHER_APP_KEY,
     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
     forceTLS: false,
     wsHost: window.location.hostname,
     wsPort: 6001
 });

window.Echo.private('events').listen('RealTimeEvent', (e) =>{
    var partida = document.getElementsByClassName("game--title");
    var jugador_2 = document.getElementsByClassName("name--two");
    const datos = JSON.parse(e.message) 
    if(datos.partida == partida[0].outerText){
        jugador_2[0].innerHTML = datos.nombre
        document.querySelectorAll('.cell').forEach(cell => cell.innerHTML = "");
        document.getElementsByClassName("game--status")[0].innerHTML = "";
        gameActiveP['X'] = true;
        limpia_pantalla('Es el turno de X')
    }
});


window.Echo.private('move').listen('NotificaJugada', (e) =>{
    const datos = JSON.parse(e.message)
    var partida = document.getElementsByClassName("game--title");
    if(datos.partida == partida[0].outerText){ 
        gameState[datos.celda] = datos.jugada
        document.getElementsByClassName("game--status")[0].innerHTML = "";
        document.getElementsByClassName("game--status")[0].innerHTML = datos.mensaje
        document.getElementsByClassName("cell")[datos.celda].innerHTML = datos.jugada
        gameActive = datos.activo === "true" ? true : false;
        gameActiveP = {
            "X" : datos.jugada == "X" ? false : true,
            "O" : datos.jugada == "O" ? false : true,
        };
        if(!gameActive){
            $(".game--restart").show();
        }
        
    }
});

window.Echo.private('restart').listen('ReiniciaPartida', (e) =>{
    const datos = JSON.parse(e.message)
    var partida = document.getElementsByClassName("game--title");
    if(datos.partida == partida[0].outerText){
        limpia_pantalla(!gameActiveP['X'] ? 'Es el turno de O' : 'Es el turno de X') 
    }
});

function limpia_pantalla(currentPlayerTurn){
    const statusDisplay = document.querySelector('.game--status')
        gameActive = true;
        gameState = ["", "", "", "", "", "", "", "", ""];
        statusDisplay.innerHTML = currentPlayerTurn
        document.querySelectorAll('.cell').forEach(cell => cell.innerHTML = "");
        $(".game--restart").hide();

}